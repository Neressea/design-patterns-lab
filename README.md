##### Projet de MOCI #####

Projet d'impl�mentation des patrons de conception.
Une classe graphique a �t� cr��e afin de pouvoir tester ind�pendamment les 
diff�rents patrons. Certains patrons ne sont pas testables par eux-m�me 
(ex. : Command), et sont associ�es � toutes les interfaces utilisant SensorView.

Afin de savoir quel(s) pattern(s) sont test�s, le nom de ceux qui sont en cours
d'utilisation s'affiche dans la console en d�but d'ex�cution. 

# Il est � noter que : 

- Certaines classes fournies non-utilis�es ont �t� supprim�es et remplac�es 
par des fonctionnalit�s de classes (Log, ReadProperties, ...).

- Le nom de toutes les classes ajout�es a comme pr�fixe "My".

- Une javadoc a �t� g�n�r�e et est accessible dans le dossier "doc". Le fichier
index.html permet d'acc�der � la page d'accueil.

# Question bonus

Pour r�pondre � la question bonus, la classe SensorView fait appel � la m�thode
statique getCommands de l'interface MyCommandInterface qui lit le fichier
commande.properties et instancie toutes les classes de commandes qu'il contient.
Le r�sultat est une liste d'instance de commandes. 
En se basant sur cette liste, SensorView cr�e des composants, leur associe des
commandes et les ajoute au menu utilisateur. On obtient ainsi un menu cr��
� la vol�e et param�trable depuis un simple fichier texte.
Afin de pouvoir g�rer facilement le nom des boutons, j'ai ajout� une variable
name dans les commandes auxquelles j'associe le nom (i.e. la partie gauche 
de la d�claration dans le fichier de properties) afin de pouvoir le 
r�cup�rer lors de la cr�ation des boutons.
