package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.MySensorAdapter;
import eu.telecomnancy.ui.ConsoleUI;

public class MyAppForAdapted {

    public static void main(String[] args) {
        ISensor sensor = new MySensorAdapter(new LegacyTemperatureSensor());
        System.out.println("ADAPTER PATTERN");
        new ConsoleUI(sensor);
    }

}
