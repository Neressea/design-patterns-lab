package eu.telecomnancy;


import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.MyFactoryInterface;
import eu.telecomnancy.ui.MainWindow;

public class MySwingAppForFactory {
	public static void main(String[] args) {
		System.out.println("FACTORY PATTERN");
		
        MyFactoryInterface factory;
		try {
			
			//On r�cup�re le factory � instancier dans le fichier de properties
			factory = MyFactoryInterface.chooseFactory("./src/eu/telecomnancy/app.properties");
			
			ISensor sensor = factory.create();
	        MainWindow w = new MainWindow(sensor);
	        sensor.attacher(w.getView());
	        
		}catch(Exception e){
			System.out.println("Une erreur est survenue.");
			e.printStackTrace();
		}
    }
}
