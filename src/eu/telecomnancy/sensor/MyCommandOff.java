package eu.telecomnancy.sensor;

/**
 * 
 * Classe repr�sentant la commande d'extinction qui peut-�tre envoy�e � un capteur.
 * 
 * @author Vincent ALBERT
 *
 */
public class MyCommandOff implements MyCommandInterface{
	
	/**
	 * Capteur auquel s'applique la commande
	 */
	private ISensor sensor;
	
	/**
	 * Nom de la commande
	 */
	private String cmd_name;
	
	public MyCommandOff(ISensor s, String cN){
		sensor = s;
		cmd_name=cN;
	}
	
	public String getClassName() {
		return cmd_name;
	}

	/**
	 * Applique l'action correspondant � la commande envoy�e.
	 */
	@Override
	public void execute() {
		sensor.off();
	}

}
