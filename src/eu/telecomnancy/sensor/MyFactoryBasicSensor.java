package eu.telecomnancy.sensor;

/**
 * 
 * Cette classe repr�sente la fabrique de capteurs classiques.
 * 
 * @author Vincent ALBERT.
 *
 */
public class MyFactoryBasicSensor extends MyFactoryInterface{

	@Override
	/**
	 * Cr�e et retourne un capteur classique.
	 */
	public ISensor create() {
		return new TemperatureSensor();
	}

}
