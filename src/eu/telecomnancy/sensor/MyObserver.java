package eu.telecomnancy.sensor;

/**
 * 
 * Interface repr�sentant tous les observateurs du projet.
 * 
 * @author Vincent ALBERT.
 *
 */
public interface MyObserver {
	
	/**
	 * Met � jour le composant graphique en fonction du mod�le.
	 */
	public void mettreAJour();
	
	/**
	 * Renvoie l'id (num�ro d'instance) du composant.
	 * (Utilis� pour equals)
	 * 
	 * @return ID
	 */
	public int getID();
}
