package eu.telecomnancy.sensor;

/**
 * Etat du capteur lorsqu'il est �teint.
 * 
 * @author Vincent ALBERT
 *
 */
public class MySensorStateOff implements MySensorStateInterface{
	
	public MySensorStateOff() {}

	@Override
	/**
	 * La m�thode on() n'est pas utilis�e, l'�tat est d�truit lorsqu'on l'allume
	 */
	public void on() {}

	@Override
	public void off() {}

	@Override
	public boolean getStatus() {
		//Le capteur est forc�ment �teint
		return false;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		//Le capteur est forc�ment �teint
		throw new SensorNotActivatedException("Le capteur n'est pas activ�.");
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		//Le capteur est forc�ment �teint
		throw new SensorNotActivatedException("Le capteur n'est pas activ�.");
	}
	

	@Override
	public void attacher(MyObserver view) {}
	
	public void detacher(MyObserver view){}

	@Override
	public void notifier() {}
}
