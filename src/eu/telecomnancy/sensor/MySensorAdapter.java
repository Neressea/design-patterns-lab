package eu.telecomnancy.sensor;

import java.util.ArrayList;

/**
 * 
 * Classe repr�sentant un adaptateur de capteur.
 * 
 * @author Vincent ALBERT
 *
 */
public class MySensorAdapter implements ISensor{
	
	/**
	 * Capteur � adapter (adaptee)
	 */
	private LegacyTemperatureSensor sensor;
	
	/**
	 * Liste des vues observant l'adaptateur.
	 */
	private ArrayList<MyObserver> views;
	
	public MySensorAdapter(LegacyTemperatureSensor s) {
		views = new ArrayList<MyObserver>();
		sensor = s;
	}
	
	public MySensorAdapter() {
		views = new ArrayList<MyObserver>();
		sensor = new LegacyTemperatureSensor();
	}

	@Override
	/**
	 * M�thode adaptant la mise en route du capteur
	 */
	public void on() {
		//We change the status of the sensor only if it is off. 
		if(!sensor.getStatus()) sensor.onOff(); 
	}

	@Override
	/**
	 * M�thode adapatant l'extinction du capteur
	 */
	public void off() {
		//We change the status of the sensor only if it is on. 
		if(sensor.getStatus()) sensor.onOff(); 
	}

	@Override
	/**
	 * M�thode adaptant le renvoi du statut du capteur
	 */
	public boolean getStatus() {
		//The same
		return sensor.getStatus();
	}

	@Override
	/**
	 * M�thode adaptant la mise � jour du capteur
	 */
	public void update() throws SensorNotActivatedException {
		//We only check if the sensor is on. If so, there is already a thread running to change its value.
		if(!sensor.getStatus())
			throw new SensorNotActivatedException("Sensor must be activated to get its status.");
		notifier();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if(!sensor.getStatus())
			throw new SensorNotActivatedException("Sensor must be activated to get its value.");
		
		return sensor.getTemperature();
	}

	@Override
	public void attacher(MyObserver view) {
		views.add(view);
	}
	
	public void detacher(MyObserver view){
		//On red�finit equals
		views.remove(view);
	}

	@Override
	public void notifier() {
		for (int i = 0; i < views.size(); i++) {
			views.get(i).mettreAJour();
		}
	}
}
