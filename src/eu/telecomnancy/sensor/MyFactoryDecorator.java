package eu.telecomnancy.sensor;

/**
 * 
 * Cette classe repr�sente la fabrique de capteurs bas�s sur le decorator pattern.
 * 
 * @author Vincent ALBERT.
 *
 */
public class MyFactoryDecorator extends MyFactoryInterface{

	@Override
	/**
	 * Cr�e et retourne un capteur bas� sur le decorator pattern.
	 */
	public ISensor create() {
		return new MySensorDecoratorFahrenheit();
	}

}
