package eu.telecomnancy.sensor;

/**
 * 
 * Cette classe repr�sente la fabrique de capteurs bas�s sur le proxy pattern.
 * 
 * @author Vincent ALBERT.
 *
 */
public class MyFactoryProxy extends MyFactoryInterface{
	@Override
	/**
	 * Retorune un capteur proxy.
	 */
	public ISensor create() {
		return new MySensorProxy();
	}
}
