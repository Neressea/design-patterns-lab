package eu.telecomnancy.sensor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * 
 * Cette interface mod�lise les commandes du programme utilis�es pour le patron commandes.
 * Elle d�finit les m�thodes abstraites qui devront �tre impl�ment�es par toutes ses 
 * sous-classes, ainsi qu'une m�thode de chargement des commandes.
 * 
 * @author Vincent ALBERT.
 *
 */
public interface MyCommandInterface {
	
	/**
	 * Provoque l'ex�cution de la commande.
	 */
	void execute();
	
	/**
	 * R�cup�re le nom de la commande. 
	 * Il s'agit de la partie gauche dans le fichier de commandes.
	 * @return Le nom de la classe.
	 */
	String getClassName();
	
	/**
	 * Cette m�thode renvoie une liste d'instance des commandes dont le nom de la classe
	 * est contenu dans le fichier de properties commandes.properties.
	 * 
	 * @param file_path Nom du fichier de propri�t�s contenant les commandes.
	 * @param sensor Capteur auquel seront appliqu�es les commandes
	 * 
	 * @return Liste des commandes charg�es.
	 * 
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws ClassNotFoundException
	 * @throws FileNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	public static ArrayList<MyCommandInterface> getCommands(String file_path, ISensor sensor) throws NoSuchMethodException, SecurityException, ClassNotFoundException, FileNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		System.out.println("COMMAND PATTERN");
		ArrayList<MyCommandInterface> commands = new ArrayList<MyCommandInterface>();
		
		File f = new File(file_path);
		FileInputStream fis = new FileInputStream(f);
		Scanner reader = new Scanner(fis);
		String line;

		while (reader.hasNext()) {
			line = reader.nextLine();
			
			if(line.matches(".*MyCommand.*")){
				String command_class = line.split("=")[1];
				String name = line.split("=")[0];
				Class<MyCommandInterface> to_instanciate = (Class<MyCommandInterface>) Class.forName(command_class);
				Constructor<MyCommandInterface> constr = to_instanciate.getConstructor(ISensor.class, String.class);
				commands.add((MyCommandInterface)constr.newInstance(sensor, name)); 
			}
		}
		
		reader.close();
	
		return commands;
	}
}
