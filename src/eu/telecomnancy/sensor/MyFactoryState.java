package eu.telecomnancy.sensor;

/**
 * 
 * Cette classe repr�sente la fabrique de capteurs bas�s sur le state pattern.
 * 
 * @author Vincent ALBERT.
 *
 */
public class MyFactoryState extends MyFactoryInterface{

	@Override
	/**
	 * Retourne un capteur d'�tats.
	 */
	public ISensor create() {
		return new MySensorState();
	}

}
