package eu.telecomnancy.sensor;

import java.util.Random;

/**
 * Etat du capteur lorsqu'il est allum�.
 * 
 * @author Vincent ALBERT
 *
 */
public class MySensorStateOn implements MySensorStateInterface{
	double value = 0;
	
	public MySensorStateOn(){}

	@Override
	public void on() {}

	@Override
	/**
	 * Ne fait rien. Lorsqu'on �teint le capteur, on d�truit l'�tat courant.
	 */
	public void off() {}

	@Override
	public boolean getStatus() {
		//Le capteur est forc�ment allum�
		return true;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		//Le capteur est forc�ment allum�
		value = (new Random()).nextDouble() * 100;
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		//Le capteur est forc�ment allum�
		return value;
	}
	

	@Override
	public void attacher(MyObserver view) {}
	
	public void detacher(MyObserver view){}

	@Override
	public void notifier() {}
}
