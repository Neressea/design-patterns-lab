package eu.telecomnancy.sensor;


/**
 * 
 * Interface repr�sentant toutes les classes observables du projet.
 * 
 * @author Vincent ALBERT
 *
 */
public interface MyObservable {
	
	/**
	 * Ajoute un observeur � l'observ�.
	 * 
	 * @param view Observer que l'on veut ajouter
	 */
    public void attacher(MyObserver view);
    
    /**
     * Enl�ve un observeur � l'observ�.
     * Pour ce faire, on red�finit equals de MyIView.
     * 
     * @param view Observeur que l'on veut oublier
     */
    public void detacher(MyObserver view);
    
    /**
     * Notifie tous les observeurs que l'objet a subi des modifications.
     */
    public void notifier();
}
