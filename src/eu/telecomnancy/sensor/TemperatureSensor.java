package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Random;

import eu.telecomnancy.ui.MyIView;

public class TemperatureSensor implements ISensor {
    boolean state;
    double value = 0;
    private ArrayList<MyObserver> views;
    
    public TemperatureSensor() {
		views = new ArrayList<MyObserver>();
	}

    @Override
    public void on() {
        state = true;
    }

    @Override
    public void off() {
        state = false;
    }

    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (state){
            value = (new Random()).nextDouble() * 100;
            notifier();
        }else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state){
            return value;
        }else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
    
    @Override
	public void attacher(MyObserver view) {
		views.add(view);
	}
	
	public void detacher(MyObserver view){
		//On red�finit equals
		views.remove(view);
	}

	@Override
	public void notifier() {
		for (int i = 0; i < views.size(); i++) {
			views.get(i).mettreAJour();
		}
	}
}
