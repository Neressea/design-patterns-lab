package eu.telecomnancy.sensor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;

/**
 * 
 * Interface mod�lisant toutes les fabriques du programme.
 * Met � disposition une m�thode de s�lection de fabrique.
 * 
 * @author Vincent ALBERT.
 *
 */
public abstract class MyFactoryInterface {
	
	/**
	 * M�thode de cr�ation de capteurs.
	 * Instancie et retourne un capteur du type correspondant.
	 * @return Instanciation du capteur d�sir�.
	 */
	public abstract ISensor create();
	
	/**
	 * 
	 * M�thode de s�lection de la fabrique � utiliser en se basant sur le fichier app.properties.
	 * 
	 * @param file_path Chemin du fichier de propri�t�s
	 * 
	 * @return Une instance de la fabrique d�sir�e.
	 * 
	 * @throws ClassNotFoundException
	 * @throws FileNotFoundException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	public static MyFactoryInterface chooseFactory(String file_path) throws ClassNotFoundException, FileNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		MyFactoryInterface mfi = null;
		File f = new File(file_path);
		
		FileInputStream fis = new FileInputStream(f);
		Scanner reader = new Scanner(fis);
		
		String line = reader.nextLine();
		while(!line.contains("factory")) line = reader.nextLine();
		
		String factory_class = line.split("=")[1];
		
		Class to_instanciate = Class.forName(factory_class);
		Constructor constr = to_instanciate.getConstructor();
		
		mfi = (MyFactoryInterface) constr.newInstance();
		
		return mfi;
	}
}
