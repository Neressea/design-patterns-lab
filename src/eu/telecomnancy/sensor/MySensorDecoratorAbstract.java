package eu.telecomnancy.sensor;

/**
 * 
 * Classe abstraite repr�sentant les d�corateurs de capteurs du projet.
 * 
 * @author Vincent ALBERT
 *
 */
public abstract class MySensorDecoratorAbstract implements ISensor{	
	protected ISensor sensor;
	
	public MySensorDecoratorAbstract() {
		sensor = new TemperatureSensor();
	}
	
	public MySensorDecoratorAbstract(ISensor s) {
		sensor = s;
	}
		
	@Override
	public void on() {
		sensor.on();
	}

	@Override
	public void off() {
		sensor.off();
	}

	@Override
	public boolean getStatus() {
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		sensor.update();
	}

	/**
	 * Renvoie la valeur du capteur si celui-ci est allum�.
	 * La valeur est d�cor�e, c'est-�-dire que son unit� d�pend du d�corateur.
	 */
	public abstract double getValue() throws SensorNotActivatedException;
	
	/**
	 * Renvoie l'unit� utilis�e par le d�corateur (fahrenheit, celsius).
	 * Sert � l'affichage.
	 * 
	 * @return Unit� utilis�e par le capteur
	 */
	public abstract String getUnit();

	@Override
	public void attacher(MyObserver view) {
		sensor.attacher(view);
	}

	@Override
	public void detacher(MyObserver view) {
		sensor.detacher(view);
	}

	@Override
	public void notifier() {
		sensor.notifier();
	}
	
	public ISensor sensor(){
		return sensor;
	}
}
