package eu.telecomnancy.sensor;

import java.util.Date;

/**
 * 
 * Proxy de capteur. A chaque appel de m�thode, on �crit sur la console un log de la fonction appel�e
 * avec sa date et sa valeur de retour.
 * 
 * @author alber_000
 *
 */
public class MySensorProxy implements ISensor{
	private ISensor sensor;
	
	public MySensorProxy(){
		sensor = new TemperatureSensor();
	}
	
	public MySensorProxy(ISensor s){
		sensor = s;
	}
	
	@Override
	public void on() {
		log("on()", "void");
		sensor.on();
	}

	@Override
	public void off() {
		log("off()", "void");
		sensor.off();
	}

	@Override
	public boolean getStatus() {
		boolean res = sensor.getStatus();
		log("getStatus()", ""+res);
		return res;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		log("update()", "void");
		sensor.update();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		double res = sensor.getValue();
		log("getValue()", ""+res);
		return res;
	}

	@Override
	public void attacher(MyObserver view) {
		sensor.attacher(view);
	}

	@Override
	public void detacher(MyObserver view) {
		log("detacher(IView view)", "void");
		sensor.detacher(view);
	}

	@Override
	public void notifier() {
		sensor.notifier();
	}
	
	/**
	 * M�thode permettant d'�crire des logs � chaque appel de m�thode.
	 * 
	 * @param meth Chaine de caract�res repr�sentant la m�thod appel�e
	 * @param res Retour de la m�thode
	 */
	public void log(String meth, String res){
		System.out.println(new Date() + " : Appel de la m�thode --> " + meth + ", r�sultat : "+res);
	}
}
