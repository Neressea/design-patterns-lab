package eu.telecomnancy.sensor;

import java.util.ArrayList;

/**
 * 
 * Capteur utilisant contenant des �tats.
 * @author Vincent ALBERT.
 *
 */
public class MySensorState implements ISensor{
	
	private ArrayList<MyObserver> views;
	
	/**
	 * Etat du capteur � un instant donn�.
	 */
	private MySensorStateInterface state;
	
	public MySensorState() {
		System.out.println("STATE PATTERN");
		
		//A sa cr�ation, le capteur est �teint
		state = new MySensorStateOff();
		views = new ArrayList<MyObserver>();
	}

	@Override
	public void on() {
		//Quand on allume un capteur, en r�alit� on lui affecte un nouvel �tat
		state = new MySensorStateOn();
	}

	@Override
	public void off() {
		//Et quand on l'�teint, on change aussi son �tat
		state = new MySensorStateOff();
	}

	@Override
	public boolean getStatus() {
		return state.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		state.update();
		notifier();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return state.getValue();
	}

	@Override
	public void attacher(MyObserver view) {
		views.add(view);
	}
	
	public void detacher(MyObserver view){
		views.add(view);
	}

	@Override
	public void notifier() {
		state.notifier();
		
		for (int i = 0; i < views.size(); i++) {
			views.get(i).mettreAJour();;
		}
	}

}
