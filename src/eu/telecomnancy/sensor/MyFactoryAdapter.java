package eu.telecomnancy.sensor;

/**
 * 
 * Cette classe repr�sente la fabrique de capteurs bas�s sur l'adapter pattern.
 * 
 * @author Vincent ALBERT.
 *
 */
public class MyFactoryAdapter extends MyFactoryInterface{

	/**
	 * Cr�e et retourne un sensor bas� sur l'adapater pattern.
	 */
	@Override
	public ISensor create() {
		return new MySensorAdapter();
	}

}
