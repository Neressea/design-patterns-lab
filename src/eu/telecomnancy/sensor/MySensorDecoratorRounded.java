package eu.telecomnancy.sensor;

/**
 * 
 * D�corateur de capteur qui arrondit sa valeur en celsius.
 * 
 * @author Vincent ALBERT
 *
 */
public class MySensorDecoratorRounded extends MySensorDecoratorAbstract {

	public MySensorDecoratorRounded(ISensor sensor) {
		super(sensor);
	}

	@Override
	/**
	 * Arrondit la valeur du capteur.
	 */
	public double getValue() throws SensorNotActivatedException {
		return Math.round(sensor.getValue());
	}
	
	/**
	 * Dans ce cas, l'unit� est le celsius.
	 */
	public String getUnit(){
		return "�C";
	}

}
