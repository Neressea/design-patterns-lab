package eu.telecomnancy.sensor;

/**
 * 
 * D�corateur de capteur qui convertit sa valeur en fahrenheit.
 * 
 * @author alber_000
 *
 */
public class MySensorDecoratorFahrenheit extends MySensorDecoratorAbstract{

	public MySensorDecoratorFahrenheit(ISensor sensor) {
		super(sensor);
	}
	
	public MySensorDecoratorFahrenheit(){
		super(new TemperatureSensor());
	}

	@Override
	/**
	 * Calcule la valeur du capteur en fahrenheit.
	 */
	public double getValue() throws SensorNotActivatedException {
		return sensor.getValue() * 1.8 + 32;
	}
	
	/**
	 * Envoie l'unit� du capteur, dans ce cas la chaine "�F"
	 */
	public String getUnit(){
		return "�F";
	}

}
