package eu.telecomnancy.sensor;

/**
 * 
 * D�corateur de capteur qui utilise les valeurs en celsius.
 * 
 * @author Vincent ALBERT
 *
 */
public class MySensorDecoratorCelsius extends MySensorDecoratorAbstract{

	public MySensorDecoratorCelsius(ISensor sensor) {
		super(sensor);
	}
	
	public MySensorDecoratorCelsius() {
		super(new TemperatureSensor());
	}

	@Override
	/**
	 * On envoie la valeur classique du capteur.
	 */
	public double getValue() throws SensorNotActivatedException {
		return sensor.getValue();
	}

	@Override
	/**
	 * L'unit� dans ce cas est en celsius.
	 */
	public String getUnit() {
		return "�C";
	}
	
}
