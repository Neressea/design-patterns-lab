package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.MySensorProxy;
import eu.telecomnancy.ui.MainWindow;

public class MySwingAppForProxy {
	
	/**
	 * REPONSE A LA QUESTION : 
	 * 
	 * Les pattern Decorateur, Etat et Proxy contiennent tous une instance concr�te 
	 * de la classe/interface dont ils h�ritent afin de modifier/g�rer son comportement.
	 */
	
	public static void main(String[] args) {
        ISensor sensor = new MySensorProxy();
        System.out.println("PROXY PATTERN");
        MainWindow w = new MainWindow(sensor);
        sensor.attacher(w.getView());
    }
	
}
