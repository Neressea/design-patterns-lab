package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.MyCommandInterface;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class SensorView extends JPanel implements MyIView{
    private ISensor sensor;
    private int id;
    private ArrayList<MyCommandInterface> commands;
    public static int lastID = 0;

    private JLabel value = new JLabel("N/A °C");
    private ArrayList<JButton> buttons;
    
    public SensorView(ISensor c) {
    	System.out.println("OBSERVER OBSERVABLE PATTERN");
    	
    	id = lastID++;
    	buttons = new ArrayList<JButton>();
    	
    	this.sensor = c;
    	commands = null;
    	
    	try{
    		commands = MyCommandInterface.getCommands("./src/eu/telecomnancy/commande.properties", sensor);
    	}catch(Exception e){
    		e.printStackTrace();
    		Runtime.getRuntime().exit(ERROR);
    	}
    	
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);
        
        for (int i = 0; i < commands.size(); i++) {
        	
        	MyCommandInterface mci = commands.get(i);
        	JButton jb = new JButton(mci.getClassName());
        	jb.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					mci.execute();
				}
			});
        	
			buttons.add(jb);
		}

        
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        for(int i=0; i<buttons.size(); i++) buttonsPanel.add(buttons.get(i));
        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

	@Override
	public void mettreAJour() {
		try {
			value.setText(sensor.getValue()+"�C");
		} catch (SensorNotActivatedException e) {
			value.setText("An error occured");
		}
	}
	
	public int getID(){
		return id;
	}
	
	@Override
	public boolean equals(Object obj) {
		MyIView v = (MyIView) obj;
		
		return v.getID() == id;
	}
}
