package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.MySensorDecoratorAbstract;
import eu.telecomnancy.sensor.MySensorDecoratorFahrenheit;
import eu.telecomnancy.sensor.MySensorDecoratorCelsius;
import eu.telecomnancy.sensor.MySensorDecoratorRounded;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe graphique sp�cifique aux d�corateurs de capteurs.
 * La vue doit permettre de changer le d�corateur � la vol�e.
 * Lorsqu'on change de d�corateur, on conserve tout de m�me l'ancien capteur.
 * 
 * @author Vincent ALBERT
 *
 */
public class MyDecoratorSensorView extends JPanel implements MyIView{
	
	/**
	 * D�corateur utilis�
	 */
    private MySensorDecoratorAbstract sensor;
    
    /**
     * Id de l'observateur
     */
    private int id;
    
    /**
     * Dernier ID allou�e � un observateur
     */
    public static int lastID = 0;

    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private JButton cel = new JButton("Celsius");
    private JButton fahr = new JButton("Fahrenheit");
    private JButton round = new JButton("Round");

    public MyDecoratorSensorView(MySensorDecoratorAbstract c) {
    	id = lastID++;
    	System.out.println("OBSERVER OBSERVABLE PATTERN");
    	
        this.sensor = c;
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sensor.on();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sensor.off();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    sensor.update();
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                }
            }
        });
        
        fahr.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//On cr�e un nouveau d�corateur bas� sur le m�me capteur.
				sensor = new MySensorDecoratorFahrenheit(sensor.sensor());
				sensor.notifier();
			}
		});
        
        round.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//On cr�e un nouveau d�corateur bas� sur le m�me capteur.
				sensor = new MySensorDecoratorRounded(sensor.sensor());
				sensor.notifier();
			}
		});
        
        cel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//On cr�e un nouveau d�corateur bas� sur le m�me capteur.
				sensor = new MySensorDecoratorCelsius(sensor.sensor());
				sensor.notifier();
			}
		});

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        buttonsPanel.add(cel);
        buttonsPanel.add(fahr);
        buttonsPanel.add(round);
   

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

	@Override
	public void mettreAJour() {
		try {
			value.setText(sensor.getValue()+sensor.getUnit());
		} catch (SensorNotActivatedException e) {
			value.setText("An error occured");
		}
	}
	
	public int getID(){
		return id;
	}
	
	@Override
	public boolean equals(Object obj) {
		MyIView v = (MyIView) obj;
		
		return v.getID() == id;
	}
}
