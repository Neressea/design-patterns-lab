package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.MyObserver;

public interface MyIView extends MyObserver{
	
	@Override
	boolean equals(Object obj);
}
