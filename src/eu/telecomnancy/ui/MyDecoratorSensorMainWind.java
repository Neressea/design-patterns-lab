package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.MySensorDecoratorAbstract;

import javax.swing.*;
import java.awt.*;

/**
 * 
 * Une vue sp�cifique aux capteurs d�cor�s. En effet, on doit fournir des boutons pour
 * changer de d�corateur � la vol�e.
 * 
 * @author Vincent ALBERT.
 *
 */
public class MyDecoratorSensorMainWind extends JFrame {

	/**
	 * D�corateur utilis�
	 */
    private MySensorDecoratorAbstract sensor;
    
    /**
     * On utilise une vue sp�cifique pour ce d�corateur.
     */
    private MyDecoratorSensorView sensorView;

    public MyDecoratorSensorMainWind(MySensorDecoratorAbstract sensor) {
        this.sensor = sensor;
        this.sensorView = new MyDecoratorSensorView(this.sensor);

        this.setLayout(new BorderLayout());
        this.add(this.sensorView, BorderLayout.CENTER);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }

    public MyIView getView(){
    	return sensorView;
    }
}