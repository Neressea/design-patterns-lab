package eu.telecomnancy;

import eu.telecomnancy.sensor.MySensorDecoratorAbstract;
import eu.telecomnancy.sensor.MySensorDecoratorCelsius;
import eu.telecomnancy.ui.MyDecoratorSensorMainWind;

public class MySwingAppForDecorateur {
	public static void main(String[] args) {
        MySensorDecoratorAbstract sensor = new MySensorDecoratorCelsius();
        System.out.println("DECORATOR PATTERN");
        
        /*Pour le decorater pattern, nous avons besoin d'une vue spéciale afin de pouvoir
         * switcher entre les décorateurs*/
        MyDecoratorSensorMainWind w = new MyDecoratorSensorMainWind(sensor);
        sensor.attacher(w.getView());
    }
}
